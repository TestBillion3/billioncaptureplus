# LineageOS 21 for Billion Capture Plus

## LineageOS 21

  1. Download twrp-zeelog-3.70.img from the repo and install TWRP. Reboot to TWRP
  2 .Wipe Vendor, System, Cache, Userdata. Reboot to TWRP
  3. Format data in TWRP. Optionally convert to F2FS - see below: Reboot to TWRP.
  4. Download this repo as zip, extract it. Then download
     LineageOS lineage-21.0-20231224-UNOFFICIAL-mido.zip from
     https://github.com/zeelog/OTA/releases
  5. Using adb push extracted repo folder to /sdcard

    adb push billioncaptureplus /sdcard/

  6. Using adb push copy LineageOS to phone

    adb push lineage-21.0-20231224-UNOFFICIAL-mido.zip /sdcard/

  7. In TWRP, using adb and run sh fix-vendor.sh

  adb shell sh /sdcard/billioncaptureplus/fix-vendor.sh

  8. Reboot to System

### LineageOS Bugs

 - Proximity sensor - disable from Phone settings
 - Autobrightness, but could be due to some glass protectors.
 - For double tap to work, phone needs to be tilted on horizontal axis, then
   touch with two fingers upper part of the screen and then lift fingers and
   double tap bottom part of the screen.
 - Do not enable Gestures or Disable Hardware Buttons from the LineageOS Wizard
 - Convert Data to F2FS if there are phone reboots

### LineageOS Cleanup

Enable rooted debugging and then run from adb shell

    pm disable com.android.dynsystem
    pm disable io.chaldeaprjkt.gamespace
    pm disable org.lineageos.setupwizard
    pm disable com.android.providers.calendar
    pm disable com.android.managedprovisioning
    pm disable com.android.statementservice
    pm disable org.omnirom.omnijaws
    pm disable org.lineageos.updater
    pm disable com.stevesoltys.seedvault
    pm disable org.calyxos.backup.contacts
    pm disable com.android.smspush
    pm disable com.android.cellbroadcastreceiver
    pm disable com.android.cellbroadcastreceiver.module
    pm disable org.lineageos.etar
    pm disable com.android.fmradio
    pm disable com.android.stk

## Compile Kernel

Download kernel from github.com/zeelog, this repo as a zip file
to the same folder, then modify and run compile.sh

Then

   dtc old-billion-captureplus.dts.dts > billion-captureplus.dtb
   cat pathto/Image.gz billion-captureplus.dtb > split-img/boot.zImage

   Repack zeelog's boot.img using AIK - https://forum.xda-developers.com/t/tool-android-image-kitchen-unpack-repack-kernel-ramdisk-win-android-linux-mac.2073775/

