#!/bin/sh

cd /sdcard/billion*

echo "Fixes for BillionCapturePlus to work with LineageOS 20240123 for Redmi Note 4x"

#ln -s /dev/block/bootdevice/by-name/vendor /dev/block/bootdevice/by-name/cust
#ln -s /dev/block/by-name/vendor /dev/block/by-name/cust

twrp install /sdcard/lineage-21.0-20240123-UNOFFICIAL-mido.zip

#patch kernel
dd if=$PWD/boot-14.img of=/dev/block/bootdevice/by-name/boot bs=10M

mount /dev/block/bootdevice/by-name/vendor /vendor
# fix gesture

# copy camera files
rm -rf /vendor/etc/camera
cp -r $PWD/new-vendor-fixes/camera /vendor/etc/
cp $PWD/new-vendor-fixes/camera-fixes/*9800w* /vendor/lib/
cp $PWD/new-vendor-fixes/camera-fixes/*s5k4h8* /vendor/lib/
cp $PWD/new-vendor-fixes/camera-fixes/*eeprom* /vendor/lib/


# audio fixes
rm -rf /vendor/etc/mixer_paths_mtp.xml
cp $PWD/new-vendor-fixes/mixer_paths.xml /vendor/etc/mixer_paths.xml
cp /vendor/etc/audio_platform_info_intcodec.xml /vendor/etc/audio_platform_info.xml

# fix fingerprint

cp $PWD/new-vendor-fixes/*finger*rc /vendor/etc/init
chmod -w /vendor/etc/init/*finger*rc
# copy chipone fingerpint.default.so (from nokia) and replace mido's fpc
cp $PWD/new-vendor-fixes/*finger*so /vendor/lib64/hw
chmod -w /vendor/lib64/hw/*finger*so

echo "sys.fp.vendor=default" >> /vendor/build.prop
echo "fpc.fp.miui.token=0" >> /vendor/build.prop
echo "ro.boot.fpsensor=default" >> /vendor/build.prop
echo "ro.hardware.fingerprint=default" >> /vendor/build.prop

# copy fp firmware
cp $PWD/new-vendor-fixes/firmware/* /vendor/firmware

#fix fp selinux
PWD1=$PWD

cd /vendor
sed -i "s#/dev/goodix_fp#/dev/fpsensor#g" /vendor/etc/selinux/vendor_file_contexts
sed -i "s#gx_fpd_device#uhid_device#g" /vendor/etc/selinux/vendor_file_contexts

sed -i -e '/chown system system \/sys\/devices\/soc\/78b7000.i2c\/i2c-3\/3-0038\/enable_dt2w/a\' -e '   chown system system /sys/devices/soc/78b7000.i2c/i2c-3/3-0014/enable_dt2w' /vendor/etc/init/hw/init.qcom.rc
sed -i -e '/chmod 0664 \/sys\/devices\/soc\/78b7000.i2c\/i2c-3\/3-0038\/enable_dt2w/a\' -e '   chmod 0664 /sys/devices/soc/78b7000.i2c/i2c-3/3-0014/enable_dt2w' /vendor/etc/init/hw/init.qcom.rc

restorecon /vendor/firmware/*
restorecon /vendor/lib64/hw/finger*

#disable zram
patch -d /vendor/bin < $PWD/patches/disable-zram.patch

cd /

#fix userdata crash

umount /dev/block/bootdevice/by-name/vendor
umount /dev/block/bootdevice/by-name/vendor
tune2fs -O ^has_journal /dev/block/bootdevice/by-name/vendor
e2fsck -pv /dev/block/bootdevice/by-name/vendor

umount /dev/block/bootdevice/by-name/system
umount /dev/block/bootdevice/by-name/system
tune2fs -O ^has_journal /dev/block/bootdevice/by-name/system
e2fsck -pv /dev/block/bootdevice/by-name/system

umount /dev/block/bootdevice/by-name/userdata
tune2fs -O ^has_journal /dev/block/bootdevice/by-name/userdata
umount /dev/block/bootdevice/by-name/system

umount /dev/block/bootdevice/by-name/persist
umount /dev/block/bootdevice/by-name/persist
tune2fs -O ^has_journal /dev/block/bootdevice/by-name/persist
e2fsck -pv /dev/block/bootdevice/by-name/persist



echo "DONE!"


