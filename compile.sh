#!/bin/sh

MIDO_PATH=$HOME/android_kernel_xiaomi_mido-lineage-21.0
BILLION_PATH=$HOME/billioncaptureplus/android-kernel-changes

cd ..

chmod +x scripts/*
rm -rf $MIDO_PATH/drivers/input/touchscreen/gt9xx_v2.4/
cp -r $BILLION_PATH/drivers/input/touchscreen/gt9xx_v2.4/ $MIDO_PATH/drivers/input/touchscreen/
cp -r $BILLION_PATH/drivers/misc/fpsensor_chipone/ $MIDO_PATH/drivers/misc/

cd $MIDO_PATH

patch -Np1  < $BILLION_PATH/patches/fix-comp1.patch
patch -Np1  < $BILLION_PATH/patches/fix-comp2.patch
patch -Np1  < $BILLION_PATH/patches/fix-comp3.patch
patch -Np1  < $BILLION_PATH/patches/chipone-fingerprint-enable.patch


export PATH="/usr/bin"
export PATH="$HOME/aosp-clang/clang-r450784d/bin:$PATH"
export CLANG_TRIPLE=aarch64-linux-gnu-
export ARCH=arm64 && export SUBARCH=arm64

export CROSS_COMPILE=aarch64-linux-gnu-

make mrproper CC=clang \
	AR=llvm-ar \
	NM=llvm-nm \
	OBJCOPY=llvm-objcopy \
	OBJDUMP=llvm-objdump \
	READELF=llvm-readelf \
	OBJSIZE=llvm-size \
	STRIP=llvm-strip \
	HOSTCC=clang \
	HOSTCXX=clang++

make vendor/mido_defconfig CC=clang \
	AR=llvm-ar \
	NM=llvm-nm \
	OBJCOPY=llvm-objcopy \
	OBJDUMP=llvm-objdump \
	READELF=llvm-readelf \
	OBJSIZE=llvm-size \
	STRIP=llvm-strip \
	HOSTCC=clang \
	HOSTCXX=clang++ CROSS_COMPILE=aarch64-linux-gnu-

make -j10 CC=clang \
  LD=ld.lld \
	AR=llvm-ar \
	NM=llvm-nm \
	OBJCOPY=llvm-objcopy \
	OBJDUMP=llvm-objdump \
	READELF=llvm-readelf \
	OBJSIZE=llvm-size \
	STRIP=llvm-strip \
	HOSTCC=clang \
	HOSTCXX=clang++ CROSS_COMPILE=aarch64-linux-gnu-
